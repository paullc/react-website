/* eslint-disable no-unused-vars */
import React, { Component } from "react";
import mapboxgl from "mapbox-gl";
import axios from "axios";
import { CSSTransition, TransitionGroup } from "react-transition-group";
require("dotenv").config();

const locData = require("../Data/locations.json");

export class Map extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lat: 32.425222,
            lng: -60.282423,
            zoom: 1.4,
            lastLocation: "",
            isDisplayed: false,
            data: ""
        };
    }

    componentDidMount() {
        // axios
        //     .get(`http://localhost:3001/paulocations/getall`)
        //     .then(response => {
        //         console.log(response);
        //         this.setState({data: response.data});
        //         this.createMap();
        //     })
        //     .catch(error => {
        //         console.log(error);
                
        //     });
        this.createMap();
    }

    render() {
        return (
            <div className="map">
                <h3 id="mapMsg">Here's a Map of All My Travels!</h3>
                <div
                    ref={el => (this.mapContainer = el)}
                    className="mapContainer"
                ></div>
                {this.state.isDisplayed && (
                    <CSSTransition
                        in={this.state.isDisplayed}
                        appear={true}
                        timeout={2000}
                        classNames="location-animation"
                    >
                        <div>
                            <h3 id="dynamicLocation">
                                What Did I Do In {this.state.lastLocation}?
                            </h3>
                            <div id="locInfo">
                                <p id="locDesc"></p>
                            </div>
                        </div>
                    </CSSTransition>
                )}
            </div>
        );
    }

    displayLocationInfo = index => {
        if (document.getElementById("locInfo") != null) {
            document.getElementById("locDesc").innerHTML = locData[
                index
            ].desc;
        }
    };

    createMap = () => {
        mapboxgl.accessToken =
            "pk.eyJ1IjoicG9wdXA0dDQiLCJhIjoiY2s0YXd5Z2JhMDd4ejNncnFmbHIwcTl1NCJ9.GWC1__7Mfig4rM2WbEDfvQ";

        const map = new mapboxgl.Map({
            container: this.mapContainer,
            style: "mapbox://styles/popup4t4/ck5awa74p060g1cqa6r07xq2w", // Black
            center: [this.state.lng, this.state.lat],
            zoom: this.state.zoom
        });

        for (let i = 0; i < locData.length; i++) {
            let popup = new mapboxgl.Popup({
                closeButton: false
            }).setText(
                `${locData[i].name}, ${locData[i].region}`
            );
            let marker = new mapboxgl.Marker({ color: "#ff787a" })
                .setLngLat(locData[i].coords)
                .setPopup(popup)
                .addTo(map);

            popup.on("open", () => {
                console.log(`${locData[i].name} Opened`);
                this.setState({
                    lastLocation: locData[i].name,
                    isDisplayed: true
                });
                this.displayLocationInfo(i);
            });

            popup.on("close", () => {
                console.log(`${locData[i].name} Closed`);

                if (this.state.lastLocation === locData[i].name) {
                    this.setState({ isDisplayed: false });
                }
            });
        }

        // map.on("click", () => {
        //     console.log(this.state.isDisplayed);
        // });
    };
}

export default Map;
