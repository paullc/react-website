import React, { Component } from "react";
import NavBar from "./NavBar";
import About from "./About.jsx";
import Home from "./Home.jsx";
import Projects from "./Projects";
import Message from "./Message";
import Links from "./Links";
import Map from "./Map";

export class App extends Component {
    state = {
        view: "HOME",
        viewLC: "Home",
        viewDesc: "Home"
    };

    render() {
        return (
            <div className="App">
                <div className="Greeting">
                    <NavBar viewing={this.viewing} />
                    <div className="Container">
                        <Message
                            viewLC={this.state.viewLC}
                            viewDesc={this.state.viewDesc}
                        />

                        <Links />
                    </div>
                </div>

                {this.state.view === "ABOUT" && <About animateGrid={true} />}
                {this.state.view === "HOME" && <Home animateGrid={true} />}
                {this.state.view === "PROJECTS" && (
                    <Projects animateGrid={true} />
                )}

                <Map />

                <footer>
                    Designed by Pau Lleonart Calvo, 2020
                    <br />
                    Using React.js, Node.js, MongoDB, Express, JWT, HTML, CSS
                </footer>
            </div>
        );
    }

    viewing = viewChoice => {
        let viewLC = "";
        let viewDesc = "";
        if (viewChoice === "HOME") {
            viewLC = "Home";
            viewDesc = "HOME";
        } else if (viewChoice === "ABOUT") {
            viewLC = "About";
            viewDesc = "ABOUT";
        } else {
            viewLC = "Projects";
            viewDesc = "PROJECTS";
        }
        this.setState({ view: viewChoice, viewLC: viewLC, viewDesc: viewDesc });
    };
}

export default App;
