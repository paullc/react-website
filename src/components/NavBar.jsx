import React, { Component } from "react";

export class NavBar extends Component {
    render() {
        return (
            <div className="navbar">
                <ul className="nav">
                    <li onClick={() => this.props.viewing("ABOUT")}>ABOUT</li>
                    <li onClick={() => this.props.viewing("HOME")}>HOME</li>
                    <li onClick={() => this.props.viewing("PROJECTS")}>
                        PROJECTS
                    </li>
                </ul>
            </div>
        );
    }
}

export default NavBar;
