import React, { Component } from "react";
import { CSSTransition} from "react-transition-group";

export class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            animate: true
        };
    }
    render() {
        return (
            <div className="page-content" id="home">
                <div className="grid-container">
                    <CSSTransition
                        in={this.props.animateGrid}
                        appear={true}
                        timeout={2000}
                        classNames="grid-animation"
                    >
                        <div
                            className="grid-item"
                            id="classes"
                            style={{ transitionDelay: "0000ms" }}
                        >
                            <h3>Classes</h3>
                        </div>
                    </CSSTransition>
                    <CSSTransition
                        in={this.props.animateGrid}
                        appear={true}
                        timeout={2000}
                        classNames="grid-animation"
                    >
                        <div
                            className="grid-item"
                            id="skills"
                            style={{ transitionDelay: "1000ms" }}
                        >
                            <h3>Skills</h3>
                            <div></div>
                        </div>
                    </CSSTransition>
                    <CSSTransition
                        in={this.props.animateGrid}
                        appear={true}
                        timeout={2000}
                        classNames="grid-animation"
                    >
                        <div
                            className="grid-item"
                            id="experience"
                            style={{ transitionDelay: "2000ms" }}
                        >
                            <h3>Experience</h3>
                        </div>
                    </CSSTransition>
                </div>
            </div>
        );
    }
}

export default Home;
