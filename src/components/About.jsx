import React, { Component } from "react";
import { CSSTransition } from "react-transition-group";

export class About extends Component {
    render() {
        return (
            <div className="page-content" id="About">
                <div className="grid-container">
                    <CSSTransition
                        in={this.props.animateGrid}
                        appear={true}
                        timeout={2000}
                        classNames="grid-animation"
                    >
                        <div
                            className="grid-item"
                            id="travel"
                            style={{ transitionDelay: "0000ms" }}
                        >
                            <h3>Travel</h3>
                        </div>
                    </CSSTransition>
                    <CSSTransition
                        in={this.props.animateGrid}
                        appear={true}
                        timeout={2000}
                        classNames="grid-animation"
                    >
                        <div
                            className="grid-item"
                            id="bio"
                            style={{ transitionDelay: "1000ms" }}
                        >
                            <h3>Bio</h3>
                        </div>
                    </CSSTransition>
                    <CSSTransition
                        in={this.props.animateGrid}
                        appear={true}
                        timeout={2000}
                        classNames="grid-animation"
                    >
                        <div
                            className="grid-item"
                            id="interests"
                            style={{ transitionDelay: "2000ms" }}
                        >
                            <h3>Interests</h3>
                        </div>
                    </CSSTransition>
                </div>
            </div>
        );
    }
}

export default About;
