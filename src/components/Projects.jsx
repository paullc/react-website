import React, { Component } from "react";
import { CSSTransition } from "react-transition-group";

export class Projects extends Component {
    constructor(props) {
        super(props);
        this.state = {
            spotify: false,
            soundCloud: true
        };
    }

    componentDidMount() {}

    render() {
        return (
            <div className="page-content" id="Projects">
                <div className="grid-container">
                    <CSSTransition
                        in={this.props.animateGrid}
                        appear={true}
                        timeout={2000}
                        classNames="grid-animation"
                    >
                        <div
                            className="grid-item"
                            id="university"
                            style={{ transitionDelay: "0000ms" }}
                        >
                            <h3>University</h3>
                        </div>
                    </CSSTransition>
                    <CSSTransition
                        in={this.props.animateGrid}
                        appear={true}
                        timeout={2000}
                        classNames="grid-animation"
                    >
                        <div
                            className="grid-item"
                            id="personal"
                            style={{ transitionDelay: "1000ms" }}
                        >
                            <h3>Personal</h3>
                            <div></div>
                        </div>
                    </CSSTransition>
                    <CSSTransition
                        in={this.props.animateGrid}
                        appear={true}
                        timeout={2000}
                        classNames="grid-animation"
                    >
                        <div
                            className="grid-item"
                            id="music"
                            style={{ transitionDelay: "2000ms" }}
                        >
                            <div>
                                <h3
                                    id="musicChoice"
                                    onClick={() => {
                                        this.setState({
                                            spotify: !this.state.spotify,
                                            soundCloud: !this.state.soundCloud
                                        });
                                    }}
                                >
                                    {this.state.soundCloud === true
                                        ? "My Music"
                                        : "Favorite Music"}
                                </h3>
                            </div>
                            {this.state.soundCloud && (
                                <div className="music">
                                    <iframe
                                        title="Galactikush"
                                        width="100%"
                                        height="140"
                                        scrolling="no"
                                        frameBorder="no"
                                        allow="autoplay"
                                        src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/693013915&color=%23fc7c64&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"
                                    ></iframe>
                                    <iframe
                                        title="Clsr2u"
                                        width="100%"
                                        height="140"
                                        scrolling="no"
                                        frameBorder="no"
                                        allow="autoplay"
                                        src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/516232260&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"
                                    ></iframe>
                                    <iframe
                                        title="DameDolla"
                                        width="100%"
                                        height="140"
                                        scrolling="no"
                                        frameBorder="no"
                                        allow="autoplay"
                                        src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/689504383&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"
                                    ></iframe>
                                    <iframe
                                        title="dame2"
                                        width="100%"
                                        height="150"
                                        scrolling="no"
                                        frameBorder="no"
                                        allow="autoplay"
                                        src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/214558379&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"
                                    ></iframe>
                                    <iframe
                                        title="dame3"
                                        width="100%"
                                        height="140"
                                        scrolling="no"
                                        frameBorder="no"
                                        allow="autoplay"
                                        src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/247415863&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"
                                    ></iframe>
                                </div>
                            )}
                            {this.state.spotify && (
                                <div className="music">
                                    <iframe
                                        title="newPerson"
                                        src="https://open.spotify.com/embed/track/52ojopYMUzeNcudsoz7O9D"
                                        width="100%"
                                        height="80"
                                        frameBorder="0"
                                        allowtransparency="true"
                                        allow="encrypted-media"
                                    ></iframe>
                                    <iframe
                                        title="moneyTrees"
                                        src="https://open.spotify.com/embed/track/2HbKqm4o0w5wEeEFXm2sD4"
                                        width="100%"
                                        height="80"
                                        frameBorder="0"
                                        allowtransparency="true"
                                        allow="encrypted-media"
                                    ></iframe>
                                    <iframe
                                        title="lessIKnow"
                                        src="https://open.spotify.com/embed/track/6K4t31amVTZDgR3sKmwUJJ"
                                        width="100%"
                                        height="80"
                                        frameBorder="0"
                                        allowtransparency="true"
                                        allow="encrypted-media"
                                    ></iframe>
                                    <iframe
                                        title="adhd"
                                        src="https://open.spotify.com/embed/track/2MYl0er3UZ1RlKwRb5LODh"
                                        width="100%"
                                        height="80"
                                        frameBorder="0"
                                        allowtransparency="true"
                                        allow="encrypted-media"
                                    ></iframe>
                                    <iframe
                                        title="theBox"
                                        src="https://open.spotify.com/embed/track/0nbXyq5TXYPCO7pr3N8S4I"
                                        width="100%"
                                        height="80"
                                        frameBorder="0"
                                        allowtransparency="true"
                                        allow="encrypted-media"
                                    ></iframe>
                                    <iframe
                                        title="bankAccount"
                                        src="https://open.spotify.com/embed/track/2fQrGHiQOvpL9UgPvtYy6G"
                                        width="100%"
                                        height="80"
                                        frameBorder="0"
                                        allowtransparency="true"
                                        allow="encrypted-media"
                                    ></iframe>
                                    <iframe
                                        title="millions"
                                        src="https://open.spotify.com/embed/track/43YQDj1KOPMFAUcTEPKy4p"
                                        width="100%"
                                        height="80"
                                        frameBorder="0"
                                        allowtransparency="true"
                                        allow="encrypted-media"
                                    ></iframe>
                                </div>
                            )}
                        </div>
                    </CSSTransition>
                </div>
            </div>
        );
    }
}

export default Projects;
