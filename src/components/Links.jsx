/* eslint-disable jsx-a11y/anchor-has-content */
import React, { Component } from "react";

export class Links extends Component {
    render() {
        return (
            <div className="link-container">
                <ul className="links">
                    <li className="link-item">
                        <a
                            href="https://www.linkedin.com/in/paullc/"
                            className="fab fa-linkedin-in"
                            target="_blank"
                            rel="noopener noreferrer"
                        ></a>
                    </li>
                    <li className="link-item">
                        <a
                            href="mailto:paullc@vt.edu"
                            className="far fa-envelope"
                            target="_blank"
                            rel="noopener noreferrer"
                        ></a>
                    </li>
                    <li className="link-item">
                        <a
                            href="PauLleonartCalvoResume2020.pdf"
                            className="far fa-user"
                            target="_blank"
                            rel="noopener noreferrer"
                        ></a>
                    </li>
                    <li className="link-item">
                        <a
                            href="https://gitlab.com/paullc"
                            className="fab fa-gitlab"
                            target="_blank"
                            rel="noopener noreferrer"
                        ></a>
                    </li>
                </ul>
            </div>
        );
    }
}

export default Links;
