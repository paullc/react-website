const express = require("express");
const router = express.Router();
const { PauLocation, validate } = require("../models/pauLocationModel");

router.get("/getall", async (req, res) => {
    const pauLocations = await PauLocation.find({}).sort("region");
    if (!pauLocations || pauLocations.length === 0)
        return res
            .status(400)
            .send("There are no pauLocations in the database");

    res.send(pauLocations);
});

router.post("/newlocation", async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    let pauLocation = await PauLocation.find({
        name: req.body.name,
        region: req.body.region,
        desc: req.body.desc,
        coords: req.body.coords
    });

    if (pauLocation) return res.status(400).send("Location Already In Database");


    pauLocation = new PauLocation({
        name: req.body.name,
        region: req.body.region,
        desc: req.body.desc,
        coords: req.body.coords

    });
    pauLocation = await pauLocation.save();


    res.send(pauLocation);
});

module.exports = router;
