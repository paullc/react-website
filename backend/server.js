const mongoose = require("mongoose");
const express = require("express");
const cors = require("cors");
const pauLocationsRouter = require("./routes/pauLocations")
require("dotenv").config();

const app = express();
const port = process.env.PORT  || 3001;
app.use(express.json());
app.use(cors());

// --------------- MONGODB DATABASE CONNECTION -----------

mongoose
    .connect(process.env.MDB_CONN_STRING, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => console.log("Connected to MongoDB"))
    .catch(err => console.error("Could not connect to MongoDB"));

app.use("/paulocations", pauLocationsRouter);

app.listen(port, () => {
    console.log(`Server is listening on port ${port}`)
})