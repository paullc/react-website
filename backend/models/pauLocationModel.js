const mongoose = require("mongoose");
const Joi = require("joi");

const pauLocationSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    region: {
        type: String,
        required: true
    },
    desc: {
        type: String,
        required: true
    },
    coords: [
        { type: Number, required: true },
        { type: Number, required: true }
    ]
});

const PauLocation = mongoose.model("PauLocation", pauLocationSchema);

function validatePauLocation(pauLocation) {
    const schema = {
        name: Joi.string().required(),
        region: Joi.string().required(),
        desc: Joi.string().required().max(400),
        coords: Joi.array().items(Joi.number())
    };

    return Joi.validate(pauLocation, schema);
}

exports.PauLocation = PauLocation;
exports.validate = validatePauLocation;